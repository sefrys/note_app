import 'package:flutter/material.dart';

class ColorOption extends StatelessWidget {
  final Color color;
  final String name;
  final VoidCallback onTap;

  ColorOption({
    required this.color,
    required this.name,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Container(
        width: 40,
        height: 40,
        decoration: BoxDecoration(
          color: color,
          shape: BoxShape.circle,
        ),
      ),
      title: Text(name, style: TextStyle(fontSize: 18)),
      onTap: onTap,
    );
  }
}
