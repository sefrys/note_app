import 'dart:ui';
import 'package:flutter/material.dart';

class Note {
  late final String title;
  late final String content;
  late final Color backgroundColor;

  Note(
      {required this.title,
      required this.content,
      required this.backgroundColor});
}
