import 'package:flutter/material.dart';
import 'package:yy/widgets/color_theme.dart'; // Jika diperlukan

import '../models/note.dart';
import '../models/color.dart';

class CreateNote extends StatefulWidget {
  final String? title;
  final String? content;
  final Color? backgroundColor;
  final Note? noteToEdit;

  CreateNote({this.title, this.content, this.backgroundColor, this.noteToEdit});
  @override
  State<CreateNote> createState() => _CreateNoteState();
}

class _CreateNoteState extends State<CreateNote> {
  List<Note> notes = [];
  late TextEditingController titleController;
  late TextEditingController contentController;

  late bool isTitleEmpty;
  late bool isContentEmpty;
  late Color _selectedColor;
  late Color _titleContentColor;

  @override
  void initState() {
    super.initState();

    // Initialize controllers and other variables
    titleController = TextEditingController(text: widget.noteToEdit?.title);
    contentController = TextEditingController(text: widget.noteToEdit?.content);

    // Set initial values for isTitleEmpty and isContentEmpty
    isTitleEmpty = titleController.text.isEmpty;
    isContentEmpty = contentController.text.isEmpty;

    // Register listeners for text changes
    titleController.addListener(_updateButtonStatus);
    contentController.addListener(_updateButtonStatus);

    _selectedColor = widget.noteToEdit?.backgroundColor ?? Colors.white;
    _titleContentColor = _selectedColor.computeLuminance() < 0.5
        ? Colors.white
        : Colors
            .black; // Menginisialisasikan warna title dan konten berdasarkan background
  }

  void _updateButtonStatus() {
    setState(() {
      isTitleEmpty = titleController.text.isEmpty;
      isContentEmpty = contentController.text.isEmpty;
    });
  }

  void _navigateToCreateNote({Note? noteToEdit}) async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => CreateNote(
          noteToEdit: noteToEdit,
        ),
      ),
    );

    if (result != null && result is Note) {
      if (noteToEdit != null) {
        // Update existing note
        final index = notes.indexOf(noteToEdit);
        setState(() {
          notes[index] = result;
        });
      } else {
        // Add new note
        setState(() {
          notes.add(result);
        });
      }
    }
  }

  void _showSnackBar(BuildContext context, String message) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(message),
      duration: Duration(seconds: 1),
    ));
  }

  void _onSaveButtonPressed() {
    String title = titleController.text;
    String content = contentController.text;

    if (!isTitleEmpty || !isContentEmpty) {
      if (widget.noteToEdit != null) {
        // Edit existing note
        final editedNote = Note(
          title: title,
          content: content,
          backgroundColor: _selectedColor,
        );
        Navigator.pop(context, editedNote);
        _editNoteSuccessSnackBar();
      } else {
        // Create new note
        final createdNote = Note(
          title: title,
          content: content,
          backgroundColor: _selectedColor,
        );
        Navigator.pop(context, createdNote);
        _createNoteSuccessSnackBar();
      }

      titleController.clear();
      contentController.clear();
    }
  }

  void _createNoteSuccessSnackBar() {
    _showSnackBar(context, 'Note created successfully!');
  }

  void _editNoteSuccessSnackBar() {
    _showSnackBar(context, 'Note edited successfully!');
  }

  @override
  void dispose() {
    titleController.dispose();
    contentController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.lightGreen,
        title: Container(
          child: const Text(
            'Create Note',
          ),
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context); // Kembali ke halaman sebelumnya
          },
        ),
      ),
      body: Container(
        color: _selectedColor,
        child: Padding(
          padding: const EdgeInsets.all(25.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TextFormField(
                controller: titleController,
                maxLines: null,
                decoration: InputDecoration(
                  hintText: 'Title',
                  hintStyle: TextStyle(
                    color: _titleContentColor,
                    fontWeight: FontWeight.w500,
                    fontSize: 18,
                  ),
                  border: InputBorder.none,
                ),
                style: TextStyle(
                  fontWeight: FontWeight.bold, // Mengatur teks menjadi tebal
                  fontSize: 18,
                  color:
                      _titleContentColor, // Gunakan warna yang disesuaikan di sini
                ),
              ),
              TextFormField(
                controller: contentController,
                maxLines: null,
                decoration: InputDecoration(
                  hintText: 'Write something here...',
                  hintStyle: TextStyle(
                    fontWeight: FontWeight.w400,
                    color: _titleContentColor,
                  ),
                  border: InputBorder.none,
                ),
                style: TextStyle(
                  fontWeight: FontWeight.w400, // Mengatur teks menjadi tebal
                  color:
                      _titleContentColor, // Gunakan warna yang disesuaikan di sini
                ),
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          SizedBox(
            width: 45,
            height: 45,
            child: FloatingActionButton(
              onPressed: () async {
                Color? selectedColor =
                    await ColorPicker.showColorPicker(context);
                if (selectedColor != null) {
                  setState(() {
                    _selectedColor = selectedColor;
                    _titleContentColor = selectedColor.computeLuminance() < 0.5
                        ? Colors.white
                        : Colors.black;
                  });
                }
              },
              child: Icon(Icons.color_lens),
              heroTag: null,
            ),
          ),
          SizedBox(height: 10),
          FloatingActionButton(
            onPressed:
                isTitleEmpty && isContentEmpty ? null : _onSaveButtonPressed,
            child: Icon(Icons.save),
            heroTag: null,
          ),
        ],
      ),
    );
  }
}
