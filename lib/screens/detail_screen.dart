import 'package:flutter/material.dart';
import '../models/note.dart';

class DetailScreen extends StatelessWidget {
  final Note note;

  DetailScreen({required this.note});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Notes'),
        backgroundColor: Colors.lightGreen,
      ),
      body: Container(
        color: Colors.white, // Warna latar belakang utama
        child: FractionallySizedBox(
          alignment: Alignment.centerLeft,
          widthFactor: 1, // Tentukan seberapa lebar bagian latar belakang
          child: Container(
            color: note
                .backgroundColor, // Gunakan warna latar belakang dari catatan
            padding: EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  note.title,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 24,
                  ),
                ),
                SizedBox(height: 16),
                Text(
                  note.content,
                  style: TextStyle(
                    fontSize: 18,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
