import 'package:flutter/material.dart';
import '../models/note.dart';
import '../widgets/action.dart';
import '../widgets/note_card.dart';
import 'create_screen.dart';
import 'detail_screen.dart';

enum ViewType { List, Grid }

class HomeScreen extends StatefulWidget {
  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<Note> notes = [];
  Color _selectedColor = Colors.green;
  ViewType _viewType = ViewType.List;

  void _navigateToCreateNote() async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => CreateNote()),
    );

    if (result != null && result is Note) {
      setState(() {
        notes.add(result);
      });
    }
  }

  void _showSnackBar(BuildContext context, String message) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(message),
      duration: Duration(seconds: 1),
    ));
  }

  void _editNote(Note noteToEdit) async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => CreateNote(noteToEdit: noteToEdit)),
    );

    if (result != null && result is Note) {
      final index = notes.indexOf(noteToEdit);
      setState(() {
        notes[index] = result;
      });
      _showSnackBar(context, 'Note edited successfully!');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.lightGreen,
        title: Text(
          "Notes",
          style: TextStyle(),
        ),
        actions: [
          if (notes.isNotEmpty)
            IconButton(
              icon: _viewType == ViewType.List
                  ? Icon(Icons.list)
                  : Icon(Icons.grid_view),
              onPressed: () {
                setState(() {
                  _viewType = _viewType == ViewType.List
                      ? ViewType.Grid
                      : ViewType.List;
                });
              },
            ),
        ],
      ),
      body: notes.isEmpty
          ? Center(
              child: Text(
                'Make your first note',
                style: TextStyle(color: Colors.black, fontSize: 14),
              ),
            )
          : _viewType == ViewType.List
              ? ListView.builder(
                  itemCount: notes.length,
                  itemBuilder: (context, index) {
                    final note = notes[index];
                    return GestureDetector(
                      onLongPress: () {
                        _showNoteOptions(note);
                      },
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => DetailScreen(note: note),
                          ),
                        );
                      },
                      child: NoteCard(
                        note: note,
                        titleContentColor:
                            note.backgroundColor.computeLuminance() < 0.5
                                ? Colors.white
                                : Colors.black,
                      ),
                    );
                  },
                )
              : GridView.builder(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                  ),
                  itemCount: notes.length,
                  itemBuilder: (context, index) {
                    final note = notes[index];
                    return GestureDetector(
                      onLongPress: () {
                        _showNoteOptions(note);
                      },
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => DetailScreen(note: note),
                          ),
                        );
                      },
                      child: NoteCard(
                        note: note,
                        titleContentColor:
                            note.backgroundColor.computeLuminance() < 0.5
                                ? Colors.white
                                : Colors.black,
                      ),
                    );
                  },
                ),
      floatingActionButton: FloatingActionButton(
        onPressed: _navigateToCreateNote,
        child: Icon(Icons.add),
      ),
    );
  }

  void _showNoteOptions(Note note) {
    ActionSheet.showNoteOptions(
      context,
      note,
      (editedNote) {
        _editNote(editedNote);
      },
      (deletedNote) {
        setState(() {
          notes.remove(deletedNote);
        });
      },
    );
  }
}
