import 'package:flutter/material.dart';
import '../models/note.dart';

class ActionSheet {
  static void showNoteOptions(BuildContext context, Note note,
      Function(Note) onEdit, Function(Note) onDelete) {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        return Container(
          height: 150,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Actions",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                    color: Colors.black,
                  ),
                ),
                Row(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(color: Colors.lightGreen.shade100),
                          color: Colors.lightGreen.shade100),
                      child: IconButton(
                        icon: Icon(
                          Icons.delete,
                          color: Colors.black,
                          size: 30,
                        ),
                        onPressed: () {
                          onDelete(note);
                          Navigator.pop(context);
                        },
                      ),
                    ),
                    SizedBox(width: 20),
                    Container(
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(color: Colors.lightGreen.shade100),
                          color: Colors.lightGreen.shade100),
                      child: IconButton(
                        icon: Icon(
                          Icons.edit,
                          color: Colors.black,
                          size: 30,
                        ),
                        onPressed: () async {
                          Navigator.pop(context);
                          onEdit(note);
                        },
                      ),
                    ),
                    SizedBox(width: 16),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
