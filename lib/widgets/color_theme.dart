import 'package:flutter/material.dart';
import '../models/color.dart';

class ColorPicker {
  static Future<Color?> showColorPicker(BuildContext context) async {
    return showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return Container(
          height: 400,
          child: Column(
            children: [
              ListTile(
                title: Text("Select a Color"),
              ),
              ColorOption(
                color: Colors.red,
                name: "Red",
                onTap: () {
                  Navigator.pop(context, Colors.red);
                },
              ),
              ColorOption(
                color: Colors.green,
                name: "Green",
                onTap: () {
                  Navigator.pop(context, Colors.green);
                },
              ),
              ColorOption(
                color: Colors.blue,
                name: "Blue",
                onTap: () {
                  Navigator.pop(context, Colors.blue);
                },
              ),
              ColorOption(
                color: Colors.yellow,
                name: "Yellow",
                onTap: () {
                  Navigator.pop(context, Colors.yellow);
                },
              ),
              ColorOption(
                color: Colors.brown,
                name: "brown",
                onTap: () {
                  Navigator.pop(context, Colors.brown);
                },
              ),
              ColorOption(
                color: Colors.grey.shade200,
                name: "None",
                onTap: () {
                  Navigator.pop(context, Colors.grey.shade200);
                },
              ),
            ],
          ),
        );
      },
    );
  }
}
