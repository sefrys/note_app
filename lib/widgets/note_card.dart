import 'package:flutter/material.dart';

import '../models/note.dart';
import '../screens/detail_screen.dart';

class NoteCard extends StatelessWidget {
  final Note note;
  final Color titleContentColor;

  NoteCard({required this.note, required this.titleContentColor});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => DetailScreen(note: note),
          ),
        );
      },
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          padding: EdgeInsets.all(20),
          decoration: BoxDecoration(
            color: note.backgroundColor,
            borderRadius: BorderRadius.all(Radius.circular(15)),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                note.title,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                    color: titleContentColor),
                textAlign: TextAlign.start,
                maxLines: 2, // Batasi teks menjadi 1 baris
                overflow: TextOverflow.ellipsis,
              ),
              SizedBox(height: 5),
              Text(
                note.content,
                style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 16,
                    color: titleContentColor),
                textAlign: TextAlign.start,
                maxLines: 3, // Batasi teks menjadi 1 baris
                overflow: TextOverflow.ellipsis,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
